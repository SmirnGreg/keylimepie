"""Module for [SLURM](https://slurm.schedmd.com/) interface"""

import datetime
import os
import shutil
import socket
from typing import Iterable

import numpy as np

from keylimepie import run_model


def run(
        molecule: str = 'HCO+',
        moldatfile: str = None,
        name_suffix: str = '',
        headerfile: str = None,
        limepath: str = None,
        transitions: Iterable[int] = (1,),
        incl_deg: Iterable[float] = (0,),
        nmodels: int = 50,
        pIntensity: float = 1e5,
        nThreads: int = 1,
        velres_ms: float = 20.,
        distance_pc: float = 100.,
        mstar_msun: float = 0.5,
        nchan: int = 501,
        imgres_arcsec: int = 0.1,
        cmdprefix: str = 'srun -n 1 --mem=2G',
        dust=None,
        **kwargs,
):
    if limepath is None:
        limepath = shutil.which("keylime")
        if limepath is None:
            raise FileNotFoundError(
                "`keylime` not found! Provide path to `lime` installation folder with `limepath=/path/to/lime`, and/or put `keylime` there."
            )
    os.environ['PATH'] += ":" + limepath
    if moldatfile is None:
        moldatfile = molecule.lower() + '.dat'
    if headerfile is None:
        headerfile = f'{molecule}_structure.h'

    print("Running lime")
    print("With keylimepie by Richard Teague and Grigorii Smirnov-Pinchukov")
    print(f"Start job {os.environ['SLURM_JOB_ID']} on {socket.gethostname()} at: ", datetime.datetime.today())

    run_model(
        headerfile=headerfile,
        name=f"{molecule}{name_suffix}_{os.environ['SLURM_JOB_ID']}",
        moldatfile=moldatfile,
        transitions=transitions,
        incl=np.deg2rad(incl_deg),
        nmodels=nmodels,
        pIntensity=pIntensity,
        nThreads=nThreads,
        velres=velres_ms,  # m/s
        distance=distance_pc,  # pc
        mstar=mstar_msun,  # M Sun
        nchan=nchan,
        imgres=imgres_arcsec,
        cmdprefix=cmdprefix,
        dust=dust,
        #  cmdsuffix='&',
        **kwargs
    )

    print("Finishing the simulations")
    print(f"Job {os.environ['SLURM_JOB_ID']} is done at {datetime.datetime.today()}")