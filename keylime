#!/bin/bash -e
# LIME main driver script
# This file is part of LIME, the versatile line modeling engine
#
# Copyright (C) 2006-2014 Christian Brinch
# Copyright (C) 2015-2017 The LIME development team

export PATHTOLIME=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
vernum=`grep VERSION ${PATHTOLIME}/src/lime.h` | awk '{print $3}' | sed s/\"//g

function version {
    echo "This is LIME, The versatile line modeling engine, version $vernum"
    echo "Copyright (C) 2006-2014 Christian Brinch"
    echo "Copyright (C) 2015-2017 The LIME development team"
}

function usage {
    echo "Usage: lime [OPTIONS] [[FILE]]"
    echo " "
    echo "Arguments:"
    echo "   FILE   C model file"
    echo " "
    echo "Options:"
    echo "   -V           Display version information"
    echo "   -h           Display this message"
    echo "   -f           Use fast exponential computation"
    echo "   -s           Suppress output messages"
    echo "   -t           Compile with debugging and fixed RNG seeds"
    echo "   -n           Turn off ncurses output"
    echo "   -l LIMEID    Run later, just prepare the executable. LIMEID is a prefix of output file"
    echo "   -p NTHREADS  Run in parallel with NTHREADS threads (default: 1)"
    echo ""
    echo "See <http://lime.readthedocs.org> for more information."
    echo "Report bugs to <http://github.com/lime-rt/lime/issues>."
}

function tip {
    echo "Try 'lime -h' for more information."
}

options=":Vfnstl:p:h"
cpp_flags=""
verbose="yes"
do_test="no"
use_curses="yes"
run_later="no"
LIMEID=$$

while getopts ${options} opt; do
    case $opt in
	V)
	    version
	    exit 0
	    ;;
	h)
	    usage
	    exit 0
	    ;;
	f)
	    cpp_flags+="-DFASTEXP "
	    ;;
	n)
	    use_curses="no"
	    ;;
	p)
	    nthreads=${OPTARG}
	    if [[ $nthreads =~ ^[1-9][0-9]*$ ]]; then
		cpp_flags+="-DNTHREADS=${nthreads} "
	    else
		echo "lime: error: invalid number of threads" >&2
		tip
		exit 1
	    fi
	    ;;
	s)
	    verbose="no"
	    ;;
	t)
	    do_test="yes"
	    ;;
        l)  
            run_later="yes"
            LIMEID=${OPTARG// /}
            ;;
	\?)
	    echo "lime: error: unknown option" >&2
	    tip
	    exit 1
	    ;;
    esac
done

shift $((OPTIND-1))

if [ $# -ne 1 ]; then
    echo "lime: error: incorrect number of arguments" >&2
    tip
    exit 1
fi

# Set up the PATHTOLIME, LD_LIBRARY_PATH and WORKDIR variables
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PATHTOLIME}/lib
export WORKDIR=$PWD
# Compile the code
pushd ${PATHTOLIME} >> /dev/null

LOCKDIR=LIME_IS_COMPILING
iter=1
while :
do
   if mkdir $LOCKDIR &> /dev/null
   then
       make -s EXTRACPPFLAGS="${cpp_flags}" MODELS=$WORKDIR/$1 TARGET=$WORKDIR/lime_${LIMEID}.x DOTEST=$do_test VERBOSE=$verbose USECURSES=$use_curses # USEHDF5="yes"
       make -s limeclean
       if rm -rf $LOCKDIR
       then
           echo "$LOCKDIR succsefully removed">&2
       else
           echo "Could not remove lock dir" >&2
       fi
       break
   else
       echo lime is busy, waiting>&2
       if [ $iter -gt 10 ]; then
          echo "If you don't run other instances of lime, remove `pwd`/$LOCKDIR manually">&2
       fi
       sleep 10
       ((iter++))
   fi
done

# Run the code
popd >> /dev/null
if [ $run_later != "yes" ]; then
  ./lime_${LIMEID}.x
  rm -rf lime_${LIMEID}.x
else
  echo #!/usr/bin/env/bash
  echo export PATHTOLIME=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
  echo export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PATHTOLIME}/lib
  echo export WORKDIR=$PWD
fi

exit 0
