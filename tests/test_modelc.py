import os
import re

import numpy as np

from keylimepie import run_model
from keylimepie.model.limeclass import Model
from keylimepie.model import makemodelfile


def test_modelc_skipwhitespaces_0():
    headerfile = os.path.join(os.path.dirname(__file__), 'testdata', 'model0', 'HCO+_structure.h')
    model = Model(
        headerfile,
        "co.dat",
        name="co_test",
        transitions=[2, 3],
        incl=np.deg2rad([0, 20]),
        pIntensity=1e3,
        sinkPoints=10,
        nThreads=1,
        velres=200,
        distance=123,
        mstar=0.234,
        nchan=10,
        imgres=0.01,
        nmodels=2,
    )
    for m in range(model.nmodels):
        print('Running Model %d of %d.' % (m + 1, model.nmodels))
        modelc = makemodelfile.makeFile(m, model, save=False)
        with open(os.path.join(os.path.dirname(__file__), 'testdata', 'model0', f'model_{m}_example.c')) as fff:
            expected = re.sub(r'\s+', ' ', ''.join(fff.readlines())).strip()
        modelc_nowhitespaces = re.sub(r'\s+', ' ', ''.join(modelc)).strip()
        assert modelc_nowhitespaces == expected


def test_modelc_0():
    headerfile = os.path.join(os.path.dirname(__file__), 'testdata', 'model0', 'HCO+_structure.h')
    model = Model(
        headerfile,
        "co.dat",
        name="co_test",
        transitions=[2, 3],
        incl=np.deg2rad([0, 20]),
        pIntensity=1e3,
        sinkPoints=10,
        nThreads=1,
        velres=200,
        distance=123,
        mstar=0.234,
        nchan=10,
        imgres=0.01,
        nmodels=2,
    )
    for m in range(model.nmodels):
        print('Running Model %d of %d.' % (m + 1, model.nmodels))
        makemodelfile.makeFile(m, model)
        with open(
                os.path.join(os.path.dirname(__file__), 'testdata', 'model0', f'model_{m}_example.c')
        ) as fff:
            expected = fff.readlines()
        with open(
                f'model_{m}.c'
        ) as fff:
            actual = fff.readlines()
        assert actual == expected
        os.remove(f'model_{m}.c')


def test_modelc_1_dust():
    headerfile = os.path.join(os.path.dirname(__file__), 'testdata', 'model1_dust', 'HCO+_dust_structure.h')
    model = Model(
        headerfile,
        "co.dat",
        name="co_test",
        transitions=[2, 3],
        incl=np.deg2rad([0, 20]),
        pIntensity=1e3,
        sinkPoints=10,
        nThreads=1,
        velres=200,
        distance=123,
        mstar=0.234,
        nchan=10,
        imgres=0.01,
        nmodels=2,
        dust='jena_thin_e6.tab'
    )
    for m in range(model.nmodels):
        print('Running Model %d of %d.' % (m + 1, model.nmodels))
        makemodelfile.makeFile(m, model)
        with open(
                os.path.join(os.path.dirname(__file__), 'testdata', 'model1_dust', f'model_{m}_example.c')
        ) as fff:
            expected = fff.readlines()
        with open(
                f'model_{m}.c'
        ) as fff:
            actual = fff.readlines()
        assert actual == expected
        os.remove(f'model_{m}.c')
