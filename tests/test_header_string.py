import subprocess
import importlib
import sys
import os
import ctypes

import pytest
import numpy as np
from astropy.table.table import Table
from astropy.io.ascii import read

from keylimepie.model.writeheader import write_header_string


# from keylimepie.model.writeheader import  make_2d_header_from_astropy_table


@pytest.mark.parametrize(
    ['array', 'name', 'expected'],
    [
        [[1, 2, 3, 4, 5], 'integers',
         'const static double integers[5] = {1.000e+00, 2.000e+00, 3.000e+00, 4.000e+00, 5.000e+00};\n'],
        [[0.1, 0.2, 0.3], 'floats', 'const static double floats[3] = {1.000e-01, 2.000e-01, 3.000e-01};\n'],
        [np.array([0.1, 0.2, 0.3]), 'nparray', 'const static double nparray[3] = {1.000e-01, 2.000e-01, 3.000e-01};\n'],
    ]
)
def test_header_string(array, name, expected):
    assert write_header_string(array, name) == expected

# def test_2d_header_from_table():
#     tbl = read(
#         """a b
# 10. 20.
# 11. 22.
#         """
#     )
#     header = make_2d_header_from_astropy_table(tbl)
#     with open(os.path.join(os.path.dirname(__file__), 'test_out.h'), 'w') as fff:
#         print(header, file=fff)
#     subprocess.run(['gcc', 'test_0.c'])
#     ok = not subprocess.run(['./a.out']).returncode
#     assert ok
