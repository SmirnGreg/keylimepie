import keylimepie
import os
import pytest
import subprocess
import socket

def lime_is_available():
    try:
        limeout = subprocess.run(
            ('lime', '-h'),
            capture_output=True,
        )
        return 'Usage: lime [OPTIONS] [[FILE]]' in limeout.stdout
    except FileNotFoundError:
        return False

LIME_IS_AVAILABLE = lime_is_available()
DEVELOPMENT_COMPUTERS = {
    # 'lemon',
    'aida41011',
}

@pytest.mark.skipif(
    not (LIME_IS_AVAILABLE or socket.gethostname() in DEVELOPMENT_COMPUTERS),
    reason="lime executable is not found in PATH.")
def test_trivial_run():
    wd = os.path.dirname(__file__)
    keylimepie.run_model(
        headerfile=os.path.join(wd, '../testdata/model0/HCO+_structure.h'),
        # directory=wd,
        in_system_tmp=True,
        name='test',
        moldatfile='hco+.dat',
        trans=[5],
        incl=[0.],  # 10., 50.],
        nmodels=64,
        pIntensity=1e5,
        nThreads=1,
        velres=2000,
        distance=100,
        imgres=0.5,

    )
