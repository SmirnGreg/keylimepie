#include "lime.h"
#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "HCO+_structure.h"

void input(inputPars *par, image *img){

	par->radius = 1067.32996*AU;
	par->minScale = 1.00000*AU;
	par->pIntensity = 1000;
	par->sinkPoints = 10;
	par->sampling = 2;
	par->moldatfile[0] = "co.dat";
	par->lte_only = 1;
	par->blend = 0;
	par->antialias = 1;
	par->traceRayAlgorithm = 0;
	par->nThreads = 1;


	par->collPartIds[0] = 3;
	par->collPartIds[1] = 2;
	par->nMolWeights[0] = 1.0;
	par->nMolWeights[1] = 1.0;


	img[0].pxls = 128;
	img[0].imgres = 0.010;
	img[0].distance = 123.000*PC;
	img[0].unit = 0;
	img[0].filename = "1_0.00_0.00_0.00_2.fits";
	img[0].source_vel = 0.000;
	img[0].nchan = 10;
	img[0].velres = 200.000;
	img[0].trans = 2;
	img[0].incl = 0.000;
	img[0].posang = -1.570;
	img[0].azimuth = 0.000;

	img[1].pxls = 128;
	img[1].imgres = 0.010;
	img[1].distance = 123.000*PC;
	img[1].unit = 0;
	img[1].filename = "1_0.00_0.00_0.00_3.fits";
	img[1].source_vel = 0.000;
	img[1].nchan = 10;
	img[1].velres = 200.000;
	img[1].trans = 3;
	img[1].incl = 0.000;
	img[1].posang = -1.570;
	img[1].azimuth = 0.000;

	img[2].pxls = 128;
	img[2].imgres = 0.010;
	img[2].distance = 123.000*PC;
	img[2].unit = 0;
	img[2].filename = "1_0.35_0.00_0.00_2.fits";
	img[2].source_vel = 0.000;
	img[2].nchan = 10;
	img[2].velres = 200.000;
	img[2].trans = 2;
	img[2].incl = 0.349;
	img[2].posang = -1.570;
	img[2].azimuth = 0.000;

	img[3].pxls = 128;
	img[3].imgres = 0.010;
	img[3].distance = 123.000*PC;
	img[3].unit = 0;
	img[3].filename = "1_0.35_0.00_0.00_3.fits";
	img[3].source_vel = 0.000;
	img[3].nchan = 10;
	img[3].velres = 200.000;
	img[3].trans = 3;
	img[3].incl = 0.349;
	img[3].posang = -1.570;
	img[3].azimuth = 0.000;

}


/*
    findvalue(x, y, param) - returns interpolated value at (x, y).
    radialbounds(rad) - finds the bounding vertical slices.
    verticalbounds(alt, rpnt) - finds the bounding cells.
    linterpolate(x, x0, x1, y0, y1) - interpolates values.

    This linearally interpolates the provided grid. There is no special
    handling for points at the top of the cell. Given that this should be a
    very tenuous region in the disk it should not matter to the line emission.
*/

double linterpolate(double x, double xa, double xb, double ya, double yb){
    return (x - xa) * (yb - ya) / (xb - xa) + ya;
}


int radialbounds(double rad){
    int i;
    for (i=1; i<(4698-1); i++) {
        if ((c1arr[i] - rad) * (c1arr[i-1] - rad) <= 0.) {
            return i;
        }
    }
    return -1;
}


int verticalbounds(double alt, double rpnt){
    int i;
    for (i=1; i<(4698-1); i++) {
        if (c1arr[i] == rpnt) {
            if (alt == 0.0) {
                return i + 1;
            }
            if (c1arr[i-1] == rpnt) {
                if ((c2arr[i] - alt) * (c2arr[i-1] - alt) < 0.) {
                    return i;
                }
            }
        }
    }
    return -1;
}


double findvalue(double x, double y, double z, const double arr[4698]){

    // Coordinate transform.
    double rad = sqrt(x*x + y*y) / AU;
    double alt = fabs(z) / AU;

    // Radial bounding index.
    int ridx = radialbounds(rad);
    if (ridx < 0) {
        return -1.0;
    }
    
    // Vertical bounding indices.
    int zidx_a = verticalbounds(alt, c1arr[ridx-1]);
    int zidx_b = verticalbounds(alt, c1arr[ridx]);
    if (zidx_a < 0 || zidx_b < 0) {
        return -1.0;
    }

    // Interpolation (linear).
    double val_a = linterpolate(alt, c2arr[zidx_a-1], c2arr[zidx_a], arr[zidx_a-1], arr[zidx_a]);
    double val_b = linterpolate(alt, c2arr[zidx_b-1], c2arr[zidx_b], arr[zidx_b-1], arr[zidx_b]);
    return linterpolate(rad, c1arr[ridx-1], c1arr[ridx], val_a, val_b);

}


void density(double x, double y,double z, double *density) {

	density[0] = 0.75 * findvalue(x, y, z, dens);
	if (density[0] < 1e3) {
		density[0] = 1e3;
	}

	density[1] = 0.25 * findvalue(x, y, z, dens);
	if (density[1] < 1e3) {
		density[1] = 1e3;
	}

}


void temperature(double x, double y, double z, double *temperature) {

	temperature[0] = findvalue(x, y, z, temp);
	if (temperature[0] < 2.73) {
		temperature[0] = 2.73;
	}

	temperature[1] = 1.000 * temperature[0];

}


void abundance(double x, double y, double z, double *abundance) {

	abundance[0] = findvalue(x, y, z, abund);
	if (abundance[0] < 0.){
		abundance[0] = 0.;
	}

}


void doppler(double x, double y,double z, double *doppler) {

	*doppler = 0.000e+00;

}


void velocity(double x, double y,double z, double *velocity) {

	if (sqrt(x*x + y*y + z*z) == 0.0){
		velocity[0] = 0.0;
		velocity[1] = 0.0;
		velocity[2] = 0.0;
		 return;
	}

	double velo;
	velo = sqrt(6.67e-11 * 0.234 * 1.989e30 / sqrt(x*x + y*y + z*z));
	velocity[0] = velo * sin(atan2(y,x));
	velocity[1] = velo * cos(atan2(y,x));
	velocity[2] = 0.0;

}


