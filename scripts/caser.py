import math
from dataclasses import dataclass, field
from typing import Callable, Dict
import os
import logging
from glob import glob
import re

import numpy as np
import astropy.io.fits
from astropy import units as u
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize, LogNorm
from matplotlib.patches import Ellipse
from matplotlib.backends.backend_pdf import PdfPages
from cycler import cycler

from divan import MedianLogNorm

module_logger = logging.getLogger("keylimepie.caser")


def formula_to_latex(formula: str):
    formula = formula.replace('+', '$^+$')
    formula = formula.replace('13C', '$^{13}$C')
    formula = formula.replace('18O', '$^{18}$O')
    return formula


def continuum_subtracted(cube):
    median_layer = np.median(cube, axis=0)
    return cube - median_layer


def array_from_fits_header(header: Dict, axis: int = 0):
    return (
            (np.arange(header[f'NAXIS{axis}']) - header[f'CRPIX{axis}'] + 1)
            * header[f'CDELT{axis}']
            + header[f'CRVAL{axis}']
    )


def boxinfo(name, incl, trans, show_transition=True, show_inclination=True):
    lines = []
    lines.append(f"{formula_to_latex(name)}")
    if show_inclination: lines.append(f"{np.rad2deg(incl) % 360:.0f} deg")
    if show_transition: lines.append(f"J={trans + 1}-{trans}")
    return '\n'.join(lines)


@dataclass
class Data:
    prefix: str
    name: str
    gap: tuple = ()
    images: list = field(default_factory=list)
    colormap: str = 'afmhot'
    norm: Callable = LogNorm(1, 10)


@dataclass
class Ratio:
    name1: str
    name2: str
    name: str
    gap: tuple = ()
    colormap: str = 'coolwarm'
    norm: Callable = LogNorm(0.1, 10)


def get_filename(data: Data, settings):
    """Find file in cwd following the naming of keylimepie output"""
    prefix_no_number = re.search(r"(.*?)(?:\d+)?$", data.prefix)[1]

    filenames_ignore_number_in_prefix = glob(
        '%s*_%.2f_0.00_0.00_%d.fits' % (prefix_no_number, settings['incl'], settings['trans']))
    module_logger.debug("Found filenames following the template: %s", filenames_ignore_number_in_prefix)
    if len(filenames_ignore_number_in_prefix) > 1:
        return '%s_%.2f_0.00_0.00_%d.fits' % (data.prefix, settings['incl'], settings['trans'])
    else:
        return filenames_ignore_number_in_prefix[0]


def generate_figure(
        inclinations=(0,),
        transitions=(2,),
        workdir='.',
        outputfilename='cycler.pdf',
        datas=(),
        ratios=(),
        xlabel_everywhere=False,
        ylabel_everywhere=False,
        max_rows=np.inf,
        constrained_layout=False,
        colorbar_kwargs=None,
        separate_figs=False,
        subtract_continuum=False,
        show_transition=True,
        show_inclination=True,
        tight_layout=False,
        ignore_colorbars=(),
        bbox_inches='tight',
        size_inches=None,
):
    if colorbar_kwargs is None:
        colorbar_kwargs = {}
    curdir = os.getcwd()
    os.chdir(workdir)

    cycler_object = (
            cycler("incl", inclinations)
            * cycler("trans", transitions)
        # * cycler("prefix", prefixes)
    )

    columns = len(inclinations) * len(transitions)
    rows = len(datas) + len(ratios)

    nplots = columns * rows
    rows_columns = ()
    if rows > max_rows:
        rows_columns = (rows, columns)
        rows = max_rows
        columns = math.ceil(nplots / max_rows)

    module_logger.info("Number of plots to show: %d; in %d rows and %d columns", nplots, rows, columns)

    if not separate_figs:
        fig, ax = plt.subplots(
            rows, columns,
            # sharex=True, sharey=True,
            figsize=(columns * 4 + 1, rows * 4 + 3),
            # figsize=(columns * 2 + 0.5, rows * 2 + 1.5),
            subplot_kw={
                'aspect': 'equal',
            },
            gridspec_kw={
                'wspace': 0,
                'hspace': 0.03,
            },
            squeeze=False,
            constrained_layout=constrained_layout,
        )
        if rows_columns:
            ax = ax.reshape(*rows_columns)
            figures = [fig]
    else:
        figures = []
        # fig.subplots_adjust(wspace=0)
        # ax = ax.T
    images = []

    def plot_image(
            image,
            settings, ax,
            i, j,
            cmap='afmhot',
            name=None,
            gap=None,
            norm=None,
            size=1,
            zshift=50,
    ):
        coordminmax = (-size / 2, size / 2)
        x = np.linspace(-size / 2, size / 2, num=image.shape[0])
        y = np.linspace(-size / 2, size / 2, num=image.shape[1])
        xstep = x[1] - x[0]
        ystep = y[1] - y[0]
        x = x - xstep
        y = y - ystep
        xx, yy = np.meshgrid(x, y)
        if i == 0 or ylabel_everywhere:
            ax[j, i].set_ylabel('Offcet, [au]')
        if j == rows - 1 or xlabel_everywhere:
            ax[j, i].set_xlabel('Offcet, [au]')
        # bmp = ax[j, i].pcolormesh(
        #     xx, yy, image, norm=norm, cmap='afmhot',
        #     shading='gourand'
        # )
        bmp = ax[j, i].pcolorfast(
            coordminmax, coordminmax,
            image,
            norm=norm, cmap=cmap,
        )
        if gap:
            def plot_ellipse(radius, zshift=0, incl=0, **kwargs):
                ell_axes = (
                    2 * radius,
                    2 * radius * math.cos(incl)
                )
                ell = Ellipse(
                    (0, zshift * math.sin(incl)),
                    *ell_axes,
                    edgecolor='white', fill=False,
                    **kwargs,
                )
                ax[j, i].add_patch(ell)
                ell = Ellipse(
                    (0, zshift * math.sin(incl)),
                    *ell_axes,
                    edgecolor='black', fill=False,
                    linestyle=':',
                    **kwargs,
                )
                ax[j, i].add_patch(ell)

            plot_ellipse(gap[0], zshift=zshift, incl=settings['incl'], alpha=0.3)
            plot_ellipse(gap[1], zshift=zshift, incl=settings['incl'], alpha=0.3)
            plot_ellipse(gap[0], zshift=-zshift, incl=settings['incl'], alpha=0.3)
            plot_ellipse(gap[1], zshift=-zshift, incl=settings['incl'], alpha=0.3)

        # ax[j, i].set_title(
        #     f"{name}: {np.rad2deg(settings['incl']) % 360:.0f} deg, J={settings['trans'] + 1}-{settings['trans']}"
        # )
        textonimage = boxinfo(
            name,
            settings['incl'],
            settings['trans'],
            show_inclination=show_inclination,
            show_transition=show_transition,
        )
        ax[j, i].text(
            0.05, 0.95,
            textonimage,
            bbox={'color': 'white', 'alpha': 0.7},
            transform=ax[j, i].transAxes, verticalalignment='top'
        )
        return bmp

    for j, data in enumerate(datas):
        print(data)
        # norm = LogNorm(vmin=0.3, vmax=30)
        for i, settings in enumerate(cycler_object):
            print(settings)
            filename = get_filename(data, settings)
            # filename = '%s_%.2f_0.00_0.00_%d.fits' % (data.prefix, settings['incl'], settings['trans'])

            hdu = astropy.io.fits.open(filename, ext=0)
            cube = hdu[0].data[0]
            if subtract_continuum:
                cube = continuum_subtracted(cube)

            varr = array_from_fits_header(hdu[0].header, 3)
            image = np.trapz(cube, varr * 0.001, axis=0)
            if j > 0: sizeold = size
            size = hdu[0].header['CDELT2'] * 3600 * hdu[0].header['DISTANCE'] * hdu[0].header['NAXIS2']
            if j > 0:
                if np.abs(sizeold / size - 1) > 0.05:
                    raise NotImplementedError('Angular sizes of cubes do not match')
            # image = np.sum(cube, 0) * hdu[0].header['CDELT3'] * 0.001  # velocity resolution in km/s
            data.images.append(image)
            # images.append(image)
            if separate_figs:
                i_plot = 0
                j_plot = 0
                fig, ax = plt.subplots(
                    1, 1,
                    # sharex=True, sharey=True,
                    figsize=(4, 7),
                    subplot_kw={
                        'aspect': 'equal',
                    },
                    squeeze=False,
                )
                figures.append(fig)
            else:
                i_plot = i
                j_plot = j

            bmp = plot_image(
                image,
                settings, ax,
                i_plot, j_plot,
                name=data.name,
                gap=data.gap,
                cmap=data.colormap,
                norm=data.norm,
                size=size
            )

        if j_plot not in ignore_colorbars:
            fig.colorbar(
                bmp, ax=ax[j_plot], label="Intensity, K km s$^{-1}$",
                **colorbar_kwargs
            )

    for j, ratio in enumerate(ratios):
        print(ratio)
        # norm = MedianLogNorm(vmin=0.8, vmax=4, median=1)
        # if "DCO+/H13CO+" in ratio.name:
        #     norm = LogNorm(vmin=0.5, vmax=4)  # MedianLogNorm(vmin=0.8e-2, vmax=4e-2, median=1e-2)
        # if "DCO+/HCO+" in ratio.name:
        #     norm = LogNorm(vmin=0.05, vmax=0.4)
        for i, settings in enumerate(cycler_object):
            image1 = [data.images[i] for data in datas if data.name == ratio.name1][0]
            image2 = [data.images[i] for data in datas if data.name == ratio.name2][0]
            image_ratio = np.empty_like(image1)
            image_ratio += np.NaN
            np.divide(
                image1,
                image2,
                where=np.abs(image2) > 0.2,
                out=image_ratio,
            )
            bmp = plot_image(
                image_ratio,
                settings, ax, i, j + len(datas),
                name=ratio.name,
                gap=ratio.gap,
                cmap=ratio.colormap,
                norm=ratio.norm,
                size=size
            )

        cb = fig.colorbar(
            bmp, ax=[ax[j + len(datas)]], label="ratio",
            **colorbar_kwargs
        )
        cb.ax.minorticks_on()
    if size_inches: fig.set_size_inches(*size_inches, forward=True)
    if tight_layout:
        fig.tight_layout()
    fig.savefig(
        outputfilename,
        # dpi=800,
        overwrite=True,
        bbox_inches=bbox_inches,
    )
    os.chdir(curdir)


def generate_figure_pages(
        inclinations=(0,),
        transitions=(2,),
        workdir='.',
        outputfilename='cycler.pdf',
        datas=(),
        ratios=(),
        no_xlabel=(),
        no_ylabel=(),
        no_colorbar=(),
        colorbar_kwargs=None,
        separate_figs=False,
        subtract_continuum=False,
        show_transition=True,
        show_inclination=True,
        tight_layout=False,
        ignore_colorbars=(),
        bbox_inches='tight',
        size_inches=(3, 4),
        gridspec_kw={}
):
    if colorbar_kwargs is None:
        colorbar_kwargs = {}
    curdir = os.getcwd()
    os.chdir(workdir)

    cycler_object = (
            cycler("incl", inclinations)
            * cycler("trans", transitions)
        # * cycler("prefix", prefixes)
    )

    figures = []

    def plot_image(
            image,
            settings, ax,
            cmap='afmhot',
            name=None,
            gap=None,
            norm=None,
            size=1,
            zshift=50,
            xlabel=True,
            ylabel=True,
    ):
        coordminmax = (-size / 2, size / 2)
        x = np.linspace(-size / 2, size / 2, num=image.shape[0])
        y = np.linspace(-size / 2, size / 2, num=image.shape[1])
        xstep = x[1] - x[0]
        ystep = y[1] - y[0]
        x = x - xstep
        y = y - ystep
        xx, yy = np.meshgrid(x, y)
        if ylabel:
            ax.set_ylabel('Offcet, [au]')
        if xlabel:
            ax.set_xlabel('Offcet, [au]')
        bmp = ax.pcolorfast(
            coordminmax, coordminmax,
            image,
            norm=norm, cmap=cmap,
        )
        if gap:
            def plot_ellipse(radius, zshift=0, incl=0, **kwargs):
                ell_axes = (
                    2 * radius,
                    2 * radius * math.cos(incl)
                )
                ell = Ellipse(
                    (0, zshift * math.sin(incl)),
                    *ell_axes,
                    edgecolor='white', fill=False,
                    **kwargs,
                )
                ax.add_patch(ell)
                ell = Ellipse(
                    (0, zshift * math.sin(incl)),
                    *ell_axes,
                    edgecolor='black', fill=False,
                    linestyle=':',
                    **kwargs,
                )
                ax.add_patch(ell)

            plot_ellipse(gap[0], zshift=zshift, incl=settings['incl'], alpha=0.3)
            plot_ellipse(gap[1], zshift=zshift, incl=settings['incl'], alpha=0.3)
            plot_ellipse(gap[0], zshift=-zshift, incl=settings['incl'], alpha=0.3)
            plot_ellipse(gap[1], zshift=-zshift, incl=settings['incl'], alpha=0.3)

        # ax[j, i].set_title(
        #     f"{name}: {np.rad2deg(settings['incl']) % 360:.0f} deg, J={settings['trans'] + 1}-{settings['trans']}"
        # )
        textonimage = boxinfo(
            name,
            settings['incl'],
            settings['trans'],
            show_inclination=show_inclination,
            show_transition=show_transition,
        )
        ax.text(
            0.05, 0.95,
            textonimage,
            bbox={'color': 'white', 'alpha': 0.7},
            transform=ax.transAxes, verticalalignment='top'
        )
        return bmp

    for j, data in enumerate(datas):
        print(data)
        # norm = LogNorm(vmin=0.3, vmax=30)
        for i, settings in enumerate(cycler_object):
            print(settings)
            filename = get_filename(data, settings)
            # filename = '%s_%.2f_0.00_0.00_%d.fits' % (data.prefix, settings['incl'], settings['trans'])

            hdu = astropy.io.fits.open(filename, ext=0)
            cube = hdu[0].data[0]
            if subtract_continuum:
                cube = continuum_subtracted(cube)

            varr = array_from_fits_header(hdu[0].header, 3)
            image = np.trapz(cube, varr * 0.001, axis=0)
            if j > 0: sizeold = size
            size = hdu[0].header['CDELT2'] * 3600 * hdu[0].header['DISTANCE'] * hdu[0].header['NAXIS2']
            if j > 0:
                if np.abs(sizeold / size - 1) > 0.05:
                    raise NotImplementedError('Angular sizes of cubes do not match')
            # image = np.sum(cube, 0) * hdu[0].header['CDELT3'] * 0.001  # velocity resolution in km/s
            data.images.append(image)
            # images.append(image)

            fig, ax = plt.subplots(
                1, 1,
                # sharex=True, sharey=True,
                figsize=size_inches,
                subplot_kw={
                    'aspect': 'equal',
                },
                squeeze=True,
                gridspec_kw=gridspec_kw,
            )
            figures.append(fig)

            bmp = plot_image(
                image,
                settings, ax,
                name=data.name,
                gap=data.gap,
                cmap=data.colormap,
                norm=data.norm,
                size=size,
                ylabel=not (j in no_ylabel),
                xlabel=not (j in no_xlabel),
            )
            # print(i, not (i in no_ylabel), not (i in no_xlabel), not (i in no_colorbar))

            if not (j in no_colorbar):
                fig.colorbar(
                    bmp, ax=ax, label="Intensity, K km s$^{-1}$",
                    **colorbar_kwargs
                )

    for j, ratio in enumerate(ratios):
        print(ratio)
        # norm = MedianLogNorm(vmin=0.8, vmax=4, median=1)
        # if "DCO+/H13CO+" in ratio.name:
        #     norm = LogNorm(vmin=0.5, vmax=4)  # MedianLogNorm(vmin=0.8e-2, vmax=4e-2, median=1e-2)
        # if "DCO+/HCO+" in ratio.name:
        #     norm = LogNorm(vmin=0.05, vmax=0.4)
        for i, settings in enumerate(cycler_object):
            image1 = [data.images[i] for data in datas if data.name == ratio.name1][0]
            image2 = [data.images[i] for data in datas if data.name == ratio.name2][0]
            image_ratio = np.empty_like(image1)
            image_ratio += np.NaN
            np.divide(
                image1,
                image2,
                where=np.abs(image2) > 0.2,
                out=image_ratio,
            )
            fig, ax = plt.subplots(
                1, 1,
                # sharex=True, sharey=True,
                figsize=size_inches,
                subplot_kw={
                    'aspect': 'equal',
                },
                squeeze=True,
                gridspec_kw=gridspec_kw,
            )
            figures.append(fig)
            bmp = plot_image(
                image_ratio,
                settings, ax,
                name=ratio.name,
                gap=ratio.gap,
                cmap=ratio.colormap,
                norm=ratio.norm,
                size=size,
                ylabel=not (j + len(datas) in no_ylabel),
                xlabel=not (j + len(datas) in no_xlabel),
            )

            # cb = fig.colorbar(
            #     bmp, ax=ax, label="ratio",
            #     **colorbar_kwargs
            # )
            # cb.ax.minorticks_on()
            if not (j + len(datas) in no_colorbar):
                fig.colorbar(
                    bmp, ax=ax, label="Intensity ratio",
                    **colorbar_kwargs
                )
    with PdfPages(outputfilename) as output_file:
        for i, figure in enumerate(figures):
            # figure.set_size_inches(*size_inches, forward=True)
            if tight_layout:
                figure.tight_layout()
            output_file.savefig(figure)
    os.chdir(curdir)


if __name__ == '__main__':
    inclinations = [0, 0.35, 1.05]
    transitions = [2, 3, 4]
    # workdir = 'newhires'
    workdir = '/mnt/d/astrowork/ionization/newhires'
    if workdir == 'newdust':
        subtract_continuum = True
    else:
        subtract_continuum = False

    logging.basicConfig(level=logging.INFO)
    refmodel = "(R)"
    gaspoormodel = "(C)"
    gasrichmodel = "(D)"
    datas = [
        Data(f'HCO+nogap', f'HCO+, {refmodel}', (), norm=LogNorm(vmin=1, vmax=100)),
        Data(f'DCO+nogap', f'DCO+, {refmodel}', (), norm=LogNorm(vmin=0.3, vmax=30)),
        Data(f'H13CO+nogap', f'H13CO+, {refmodel}', (), norm=LogNorm(vmin=0.1, vmax=10)),
        Data(f'HCO+gap', f'HCO+, {gaspoormodel}', (80, 250), norm=LogNorm(vmin=1., vmax=100)),
        Data(f'DCO+gap', f'DCO+, {gaspoormodel}', (80, 250), norm=LogNorm(vmin=0.3, vmax=30)),
        Data(f'H13CO+gap', f'H13CO+, {gaspoormodel}', (80, 250), norm=LogNorm(vmin=0.1, vmax=10)),
        Data(f'HCO+dustgap', f'HCO+, {gasrichmodel}', (80, 250), norm=LogNorm(vmin=1., vmax=100)),
        Data(f'DCO+dustgap', f'DCO+, {gasrichmodel}', (80, 250), norm=LogNorm(vmin=0.3, vmax=30)),
        Data(f'H13CO+dustgap', f'H13CO+, {gasrichmodel}', (80, 250), norm=LogNorm(vmin=0.1, vmax=10)),
    ]

    ratios = [
        Ratio(f'HCO+, {gaspoormodel}', f'HCO+, {refmodel}', f'HCO+ {gaspoormodel}/{refmodel}', (80, 250),
              norm=LogNorm(0.1, 10)),
        Ratio(f'DCO+, {gaspoormodel}', f'DCO+, {refmodel}', f'DCO+ {gaspoormodel}/{refmodel}', (80, 250),
              norm=LogNorm(0.1, 10)),
        Ratio(f'H13CO+, {gaspoormodel}', f'H13CO+, {refmodel}', f'H13CO+ {gaspoormodel}/{refmodel}', (80, 250),
              norm=LogNorm(0.1, 10)),

        Ratio(f'HCO+, {gasrichmodel}', f'HCO+, {refmodel}', f'HCO+ {gasrichmodel}/{refmodel}', (80, 250),
              norm=LogNorm(0.1, 10)),
        Ratio(f'DCO+, {gasrichmodel}', f'DCO+, {refmodel}', f'DCO+ {gasrichmodel}/{refmodel}', (80, 250),
              norm=LogNorm(0.1, 10)),
        Ratio(f'H13CO+, {gasrichmodel}', f'H13CO+, {refmodel}', f'H13CO+ {gasrichmodel}/{refmodel}', (80, 250),
              norm=LogNorm(0.1, 10)),

        Ratio(f'DCO+, {refmodel}', f'HCO+, {refmodel}', f'DCO+/HCO+ {refmodel}', (), colormap='viridis',
              norm=LogNorm(1e-2, 1e0)),
        Ratio(f'DCO+, {refmodel}', f'H13CO+, {refmodel}', f'DCO+/H13CO+ {refmodel}', (), colormap='viridis',
              norm=LogNorm(1e-1, 1e1)),

        Ratio(f'DCO+, {gaspoormodel}', f'HCO+, {gaspoormodel}', f'DCO+/HCO+ {gaspoormodel}', (80, 250),
              colormap='viridis', norm=LogNorm(1e-2, 1e0)),
        Ratio(f'DCO+, {gaspoormodel}', f'H13CO+, {gaspoormodel}', f'DCO+/H13CO+ {gaspoormodel}', (80, 250),
              colormap='viridis', norm=LogNorm(1e-1, 1e1)),

        Ratio(f'DCO+, {gasrichmodel}', f'HCO+, {gasrichmodel}', f'DCO+/HCO+ {gasrichmodel}', (80, 250),
              colormap='viridis',
              norm=LogNorm(1e-2, 1e0)),
        Ratio(f'DCO+, {gasrichmodel}', f'H13CO+, {gasrichmodel}', f'DCO+/H13CO+ {gasrichmodel}', (80, 250),
              colormap='viridis',
              norm=LogNorm(1e-1, 1e1)),

        Ratio(f'H13CO+, {refmodel}', f'HCO+, {refmodel}', f'H13CO+/HCO+ {refmodel}', (), norm=LogNorm(1e-3, 1e-1)),
        Ratio(f'H13CO+, {gaspoormodel}', f'HCO+, {gaspoormodel}', f'H13CO+/HCO+ {gaspoormodel}', (80, 250),
              norm=LogNorm(1e-3, 1e-1)),
    ]

    # generate_figure(
    #     inclinations=inclinations,
    #     transitions=transitions,
    #     workdir=workdir,
    #     datas=datas,
    #     ratios=ratios,
    #     colorbar_kwargs={'shrink': 0.7},
    #     subtract_continuum=subtract_continuum,
    # )

    datas = [
        Data(f'HCO+nogap', f'HCO+, {refmodel}', (), norm=LogNorm(vmin=1, vmax=100)),
        Data(f'DCO+nogap', f'DCO+, {refmodel}', (), norm=LogNorm(vmin=0.3, vmax=30)),
        Data(f'H13CO+nogap', f'H13CO+, {refmodel}', (), norm=LogNorm(vmin=0.1, vmax=10)),
        Data(f'HCO+gap', f'HCO+, {gaspoormodel}', (80, 250), norm=LogNorm(vmin=1, vmax=100)),
        Data(f'DCO+gap', f'DCO+, {gaspoormodel}', (80, 250), norm=LogNorm(vmin=0.3, vmax=30)),
        Data(f'H13CO+gap', f'H13CO+, {gaspoormodel}', (80, 250), norm=LogNorm(vmin=0.1, vmax=10)),
        Data(f'HCO+dustgap', f'HCO+, {gasrichmodel}', (80, 250), norm=LogNorm(vmin=1., vmax=100)),
        Data(f'DCO+dustgap', f'DCO+, {gasrichmodel}', (80, 250), norm=LogNorm(vmin=0.3, vmax=30)),
        Data(f'H13CO+dustgap', f'H13CO+, {gasrichmodel}', (80, 250), norm=LogNorm(vmin=0.1, vmax=10)),
    ]
    # matplotlib.rcParams.update({'font.size': 26})
    gridspec_kw = {'left': 0.3, 'right': 0.95, 'top': 0.99, 'bottom': 0.1}
    generate_figure_pages(
        inclinations=(inclinations[0],),
        transitions=(transitions[0],),
        workdir=workdir,
        datas=datas,
        ratios=ratios[0:6],
        outputfilename='short.pdf',
        no_xlabel=(0, 1, 2, 3, 4, 5, 9, 10, 11),
        no_ylabel=(1, 2, 4, 5, 7, 8, 10, 11, 13, 14),
        no_colorbar=(0, 1, 2, 3, 4, 5, 9, 10, 11),
        # constrained_layout=True,
        colorbar_kwargs={'orientation': 'horizontal', 'shrink': 0.8},
        subtract_continuum=subtract_continuum,
        show_inclination=False, show_transition=False,
        size_inches=(2.7, 3.3),
        gridspec_kw=gridspec_kw,
        # separate_figs=True,
        # size_inches=(12, 22),
        # ignore_colorbars=(0, 1, 2, 3, 4, 5, 9, 10, 11)
        # tight_layout=True
    )
